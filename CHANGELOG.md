# changelog

* 0.1.7
  * Add line segments
  
* 0.1.6
  * Add vertical and horizontal lines

* 0.1.3
  * Remove hard-coded Python path
  * Explicit error when Python not found

* 0.1.2
  * Support for scatter plots
  * Support for scatter plot markers and opacity

* 0.1.1
  * Support for line style in plots

* 0.1.0
  * Support for line plots and markers
  * Initial commit