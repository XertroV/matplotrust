//                                         ______          _
//                   _         _       _   | ___ \        | |
//   _ __ ___   __ _| |_ _ __ | | ___ | |_ | |_/ /   _ ___| |_
//  | '_ ` _ \ / _` | __| '_ \| |/ _ \| __||    / | | / __| __|
//  | | | | | | (_| | |_| |_) | | (_) | |_ | |\ \ |_| \__ \ |_
//  |_| |_| |_|\__,_|\__| .__/|_|\___/ \__|\_| \_\__,_|___/\__|
//                      |_|
extern crate tempfile;
extern crate image;
extern crate base64;

use std::fs::File;
use std::fs;
use std::io::{Write, Read, Seek, SeekFrom};
use std::process::Command;
use tempfile::NamedTempFile;
use std::fmt;
use image::GenericImage;
use image::Pixels;
use image::Pixel;
use base64::encode;

#[cfg(test)]
#[macro_use]
extern crate probability;

#[derive(Clone)]
pub enum Markers {
    Point,
    Pixel,
    Circle,
    Triangle_Down,
    Triangle_Up,
    Triangle_Left,
    Triangle_Right,
    Tri_Down,
    Tri_Up,
    Tri_Left,
    Tri_Right,
    Square,
    Pentagon,
    Star,
    Hexagon1,
    Hexagon2,
    Plus,
    X ,
    Diamond,
    Thin_Diamond,
    VLine,
    HLine,
}

trait AsString {
    fn as_str(&self) -> &str;
}

impl Markers {
    pub fn as_str(&self) -> &str {
        match self {
            &Markers::Point => ".", 	// point marker
            &Markers::Pixel => ",", 	// pixel marker
            &Markers::Circle => "o", 	// circle marker
            &Markers::Triangle_Down => "v",  // triangle_down marker
            &Markers::Triangle_Up => "^", 	// triangle_up marker
            &Markers::Triangle_Left => "<",  //triangle_left marker
            &Markers::Triangle_Right => ">", // triangle_right marker
            &Markers::Tri_Down => "1",  // tri_down marker
            &Markers::Tri_Up => "2", // tri_up marker
            &Markers::Tri_Left => "3", // tri_left marker
            &Markers::Tri_Right => "4", // tri_right marker
            &Markers::Square => "s", // square marker
            &Markers::Pentagon => "p", // pentagon marker
            &Markers::Star => "*", // star marker
            &Markers::Hexagon1 => "h", // hexagon1 marker
            &Markers::Hexagon2 => "H", // hexagon2 marker
            &Markers::Plus => "+", // plus marker
            &Markers::X => "x", // x marker
            &Markers::Diamond => "D", // diamond marker
            &Markers::Thin_Diamond => "d", // thin_diamond marker
            &Markers::VLine => "|", // vline marker
            &Markers::HLine => "_", // hline marker
        }
    }
}

#[derive(Clone)]
pub enum LineStyle {
    Dot,
    DashDot,
    Dash,
    Fill,
}

impl LineStyle {
    pub fn as_str(&self) -> &str {
        match self {
            &LineStyle::Dot => ":",
            &LineStyle::DashDot => "-.",
            &LineStyle::Dash => "--",
            &LineStyle::Fill => "-",
        }
    }
}

#[derive(Clone)]
pub struct Figure {
    script: String,
    has_sec_y: bool,
}

#[derive(Clone)]
pub struct FigureSaveOpts {
    set_ylim: Option<f64>,
    set_ylim2: Option<f64>,
}

impl Default for FigureSaveOpts {
    fn default() -> FigureSaveOpts {
        FigureSaveOpts {
            set_ylim: None,
            set_ylim2: None,
        }
    }
}

const FALLBACK_PYTHON: &str = "/usr/local/bin/python3";


impl Figure {

    pub fn new() -> Figure {
        return Figure {
            script: "import matplotlib\nmatplotlib.use('agg')\nimport matplotlib.pyplot as plt\nfrom matplotlib.lines import Line2D\n\nfig,ax = plt.subplots(1,1)\n".to_string(),
            has_sec_y: false,
        }
    }

    pub fn add_plot(&mut self, p: String) {
        self.script += &p;
    }

    pub fn add_legend(&mut self) {
        self.script += "plt.figlegend()\n";
    }

    pub fn add_sec_y(&mut self) {
        if !self.has_sec_y {
            self.script += "ax2 = ax.twinx()\n";
            self.has_sec_y = true;
        }
    }

    pub fn add_code(&mut self, extra: String) {
        self.script += format!("{}\n", extra).as_str();
    }

    pub fn save(&mut self, output: &str, path: Option<&str>, opt: FigureSaveOpts) -> (String, String, String, String, String) {
        if let Some(yl1) = opt.set_ylim { self.add_code(format!("ax.set_ylim({})", yl1).to_string()); }
        if let Some(yl2) = opt.set_ylim2 { if self.has_sec_y { self.add_code(format!("ax2.set_ylim({})", yl2).to_string()); } }
        self.script += &format!("fig.savefig('{}')\n", output);
        // create a temporary file
        let mut tmpfile: NamedTempFile = tempfile::NamedTempFile::new().unwrap();
        tmpfile.write_all(self.script.as_bytes());

        let default_py = which::which("python3").ok().map(|p| format!("{}", p.display())).unwrap_or(FALLBACK_PYTHON.to_string());
        let python_path = match path {
            Some(s) => s,
            None => default_py.as_str()
        };

        fs::metadata(python_path).expect("python binary not found at /usr/local/bin/python3");
        let mut echo_hello = Command::new(python_path);
        echo_hello.arg(tmpfile.path());
        let py_out = echo_hello.output().expect("failed to execute process");
        let stdout = String::from_utf8(py_out.stdout).unwrap();
        let stderr = String::from_utf8(py_out.stderr).unwrap();

        (self.script.to_string(), output.to_string(), tmpfile.path().to_str().unwrap().to_string(), stdout, stderr)
    }

    pub fn as_base64(&mut self, path: Option<&str>) {
        // create a temporary file for storing the image
        let mut tmpfile: NamedTempFile = tempfile::NamedTempFile::new().unwrap();
        let filename = tmpfile.path().to_str().unwrap().to_string() + &".png".to_string();
        println!("tmpfile = {:?}", filename);
        self.save(&filename, path, FigureSaveOpts::default());
        // read the file back as a Base-64 PNG
        let mut buffer = Vec::new();
        let mut file = File::open(&filename).unwrap();
        let _out = file.read_to_end(&mut buffer);
        println!("EVCXR_BEGIN_CONTENT image/png\n{}\nEVCXR_END_CONTENT", base64::encode(&buffer));
    }

}

#[derive(Clone)]
pub struct LinePlotOptions {
    pub marker: Option<Markers>,
    pub line_style: Option<LineStyle>,
    pub color: Option<String>,
    pub alpha: Option<f64>,
    pub label: Option<String>,
    pub secondary_y: bool,
    pub zorder: Option<f64>,
    pub lw: Option<f64>,
}

impl LinePlotOptions {
    pub fn new() -> LinePlotOptions {
        return LinePlotOptions {
            marker: None,
            line_style: None,
            color: None,
            alpha: None,
            label: None,
            secondary_y: false,
            zorder: None,
            lw: None,
        }
    }
}

#[derive(Clone)]
pub struct ScatterPlotOptions {
    pub marker: Option<Markers>,
    pub alpha: Option<f64>,
}

impl ScatterPlotOptions {
    pub fn new() -> ScatterPlotOptions {
        return ScatterPlotOptions {
            marker: None,
            alpha: None,
        }
    }
}

impl fmt::Display for LinePlotOptions {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let mut options : Vec<String> = Vec::new();
        match self.marker {
            Some(ref m) => {options.push(format!("marker='{}'", m.as_str()));}
            None => {}
        }
        match self.line_style {
            Some(ref v) => {options.push(format!("linestyle='{}'", v.as_str()));}
            None => {}
        }
        match self.color {
            Some(ref v) => {options.push(format!("c='{}'", v.as_str()));}
            None => {}
        }
        if let Some(l) = &self.label { options.push(format!("label='{}'", l)); }
        if let Some(z) = &self.zorder { options.push(format!("zorder={}", z)); }
        if let Some(w) = &self.lw { options.push(format!("lw={}", w)); }
        if let Some(a) = &self.alpha { options.push(format!("alpha={:.2}", a.max(0.).min(1.))); }
        fmt.write_str(&options.join(", ")).and_then(|_| Ok(()))
    }
}

impl fmt::Display for ScatterPlotOptions {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let mut options : Vec<String> = Vec::new();
        match self.marker {
            Some(ref m) => {options.push(format!("marker='{}'", m.as_str()));}
            None => {}
        }
        match self.alpha {
            Some(ref m) => {options.push(format!("alpha={}", m.to_string()));}
            None => {}
        }
        fmt.write_str(&options.join(", ")).and_then(|_| Ok(()))
    }
}

fn convert_list_str<T>(x: Vec<T>) -> String where T: ToString {
    let mut result:Vec<String> = Vec::new();
    for _x in x { result.push(_x.to_string()); }
    return "[".to_string() + &result.join(", ") + &"]".to_string();
}

pub fn line_plot<U, T>(x: Vec<U>, y: Vec<T>, options: Option<LinePlotOptions>) -> String where U: ToString, T: ToString {
    let xs = convert_list_str::<U>(x);
    let ys = convert_list_str::<T>(y);
    let ax = match options {
        Some(LinePlotOptions { secondary_y: true, .. }) => "ax2",
        _ => "ax"
    };
    match options {
        Some(opt) => {
            return format!("{}.plot({},{},{})\n", ax, xs, ys, opt);
        },
        None => {
            return format!("{}.plot({},{})\n", ax, xs, ys);
        }
    }
}

pub fn scatter_plot<U, T>(x: Vec<U>, y: Vec<T>, options: Option<ScatterPlotOptions>) -> String where U: ToString, T: ToString {
    let xs = convert_list_str::<U>(x);
    let ys = convert_list_str::<T>(y);
        match options {
        Some(opt) => {
            return format!("ax.scatter({},{},{})\n", xs, ys, opt);
        },
        None => {
            return format!("ax.scatter({},{})\n", xs, ys);
        }
    }

}

pub fn histogram<U>(x: Vec<U>, bins: Option<u32>) -> String where U: ToString {
    let xs = convert_list_str::<U>(x);
    return format!("ax.hist({}, bins={})\n", xs, bins.unwrap_or(20));
}

pub fn horizontal_line<U>(y: U, options: Option<LinePlotOptions>) -> String where U: ToString {
    match options {
        Some(opt) => {
            return format!("ax.axhline(y={},{})\n", y.to_string(), opt);
        },
        None => {
            return format!("ax.axhline(x={})\n", y.to_string());
        }
    }
}

pub fn vertical_line<U>(x: U, options: Option<LinePlotOptions>) -> String where U: ToString {
    match options {
        Some(opt) => {
            return format!("ax.axvline(x={},{})\n", x.to_string(), opt);
        },
        None => {
            return format!("ax.axvline(x={})\n", x.to_string());
        }
    }
}

pub fn line<U, T>(start : (U, T), end : (U, T), options : Option<LinePlotOptions>) -> String where U : ToString, T : ToString {
    match options {
        Some(opt) => {
            return format!("ax=plt.gca()\nax.add_line(Line2D([{},{}],[{},{}],{}))\n", start.0.to_string(), end.0.to_string(), start.1.to_string(), end.1.to_string(), opt);
        },
        None => {
            return format!("ax=plt.gca()\nax.add_line(Line2D([{},{}],[{},{}]))\n", start.0.to_string(), end.0.to_string(), start.1.to_string(), end.1.to_string());
        }
    }
}

#[cfg(test)]
mod lineplot {
    use super::*;
    use probability::distribution::Gaussian;
    use probability::sampler::Independent;
    use probability::source;
    use ::LineStyle::Dash;

    #[test]
    fn sec_y() {
        let lp = line_plot(vec![0], vec![1], Some(LinePlotOptions { secondary_y: true, ..LinePlotOptions::new() }));
        assert!(lp.contains("ax2"));
        let lp2 = line_plot(vec![0], vec![1], Some(LinePlotOptions { secondary_y: false, ..LinePlotOptions::new() }));
        assert!(!lp2.contains("ax2"));
    }

    #[test]
    fn create_lineplot_basic() {
        let x = vec![1, 2, 3, 4];
        let y = vec![0.1, 0.2, 0.5, 0.3];
        let lp = line_plot::<i32, f64>(x, y, Some(LinePlotOptions {..LinePlotOptions::new()}));
        let mut figure = Figure::new();
        figure.add_plot(lp);
        figure.save("./examples/figures/lineplot_basic.png", None, FigureSaveOpts::default());
    }

    #[test]
    fn create_lineplot_basic_w_extras() {
        let mut x = vec![1, 2, 3, 4];
        let y = vec![0.1, 0.2, 0.5, 0.3];
        let mut figure = Figure::new();
        figure.add_sec_y();
        let lp = line_plot::<i32, f64>(x.clone(), y.clone(), Some(LinePlotOptions {label: Some("test".to_string()), lw: Some(5.0), zorder: Some(3.0), ..LinePlotOptions::new()}));
        assert_eq!(lp.contains("zorder=3"), true, "zorder param matches expected");
        assert_eq!(lp.contains("zorder=3.0"), false, "zorder param matches expected (no extra bits)");
        x.reverse();
        let lp2 = line_plot::<i32, f64>(x, y, Some(LinePlotOptions {label: Some("test2".to_string()), secondary_y: true, color: Some("green".to_string()), lw: Some(5.0), zorder: Some(1.0), alpha: Some(0.5), ..LinePlotOptions::new()}));
        figure.add_plot(lp2);
        figure.add_plot(lp);
        figure.add_legend();
        figure.save("./examples/figures/lineplot_basic_extra.png", None, FigureSaveOpts::default());
    }

     #[test]
    fn create_lineplot_basic_markers() {
        let x = vec![1, 2, 3, 4];
        let y = vec![0.1, 0.2, 0.5, 0.3];
        let mut options = LinePlotOptions::new();
        options.marker = Some(Markers::Diamond);
        let lp = line_plot::<i32, f64>(x, y, Some(options));
        let mut figure = Figure::new();
        figure.add_plot(lp);
        figure.save("./examples/figures/lineplot_basic_markers.png", None, FigureSaveOpts::default());
    }

     #[test]
    fn create_lineplot_basic_line_style() {
        let x = vec![1, 2, 3, 4];
        let y = vec![0.1, 0.2, 0.5, 0.3];
        let mut options = LinePlotOptions::new();
        options.marker = Some(Markers::Diamond);
        options.line_style = Some(LineStyle::DashDot);
        let lp = line_plot::<i32, f64>(x, y, Some(options));
        let mut figure = Figure::new();
        figure.add_plot(lp);
        figure.save("./examples/figures/lineplot_basic_linestyle.png", None, FigureSaveOpts::default());
    }

    #[test]
    fn create_scatterplot_basic() {
        let x = vec![1, 2, 3, 4];
        let y = vec![0.1, 0.2, 0.5, 0.3];
        let lp = scatter_plot::<i32, f64>(x, y, None);
        let mut figure = Figure::new();
        figure.add_plot(lp);
        figure.save("./examples/figures/scatterplot_basic.png", None, FigureSaveOpts::default());
    }

    #[test]
    fn create_scatterplot_marker() {
        let x = vec![1, 2, 3, 4];
        let y = vec![0.1, 0.2, 0.5, 0.3];
        let mut options = ScatterPlotOptions::new();
        options.marker = Some(Markers::Diamond);
        let lp = scatter_plot::<i32, f64>(x, y, Some(options));
        let mut figure = Figure::new();
        figure.add_plot(lp);
        figure.save("./examples/figures/scatterplot_marker.png", None, FigureSaveOpts::default());
    }
    #[test]
    fn create_scatterplot_marker_alpha() {
        let x = vec![1, 2, 3, 4];
        let y = vec![0.1, 0.2, 0.5, 0.3];
        let mut options = ScatterPlotOptions::new();
        options.marker = Some(Markers::Diamond);
        options.alpha = Some(0.1);
        let lp = scatter_plot::<i32, f64>(x, y, Some(options));
        let mut figure = Figure::new();
        figure.add_plot(lp);
        figure.save("./examples/figures/scatterplot_marker_alpha.png", None, FigureSaveOpts::default());
    }

    #[test]
    fn create_histogram_default_bins() {
        let mut source = source::default();
        let gaussian = Gaussian::new(0.0, 2.0);
        let mut sampler = Independent(&gaussian, &mut source);
        let x = sampler.take(500).collect::<Vec<_>>();
        let plot = histogram::<f64>(x, None);
        let mut figure = Figure::new();
        figure.add_plot(plot);
        figure.save("./examples/figures/histogram_default_bins.png", None, FigureSaveOpts::default());
    }

    #[test]
    fn create_histogram_custom_bins() {
        let mut source = source::default();
        let gaussian = Gaussian::new(0.0, 2.0);
        let mut sampler = Independent(&gaussian, &mut source);
        let x = sampler.take(500).collect::<Vec<_>>();
        let plot = histogram::<f64>(x, Some(100));
        let mut figure = Figure::new();
        figure.add_plot(plot);
        figure.save("./examples/figures/histogram_custom_bins.png", None, FigureSaveOpts::default());
    }

    #[test]
    fn create_histogram_to_b64() {
        let mut source = source::default();
        let gaussian = Gaussian::new(0.0, 2.0);
        let mut sampler = Independent(&gaussian, &mut source);
        let x = sampler.take(500).collect::<Vec<_>>();
        let plot = histogram::<f64>(x, Some(100));
        let mut figure = Figure::new();
        figure.add_plot(plot);
        figure.as_base64(None);
    }

    fn mean(numbers: &Vec<f64>) -> f64 {

        let sum: f64 = numbers.iter().sum();

        sum / numbers.len() as f64

    }

    #[test]
    fn create_histogram_vertical_mean() {
        let mut source = source::default();
        let gaussian = Gaussian::new(0.0, 2.0);
        let mut sampler = Independent(&gaussian, &mut source);
        let x = sampler.take(500).collect::<Vec<_>>();
        let plot = histogram::<f64>(x.clone(), Some(100));
        let mut figure = Figure::new();
        figure.add_plot(plot);
        let mean = mean(&(x.clone()));
        let mut mean_line_opts = LinePlotOptions::new();
        mean_line_opts.line_style = Some(Dash);
        mean_line_opts.color = Some("black".to_string());
        let mean_line = vertical_line(mean, Some(mean_line_opts));
        figure.add_plot(mean_line);
        figure.save("./examples/figures/histogram_mean_line.png", None, FigureSaveOpts::default());
    }

    #[test]
    fn create_horizontal_line() {
        let mut source = source::default();
        let gaussian = Gaussian::new(0.0, 2.0);
        let mut sampler = Independent(&gaussian, &mut source);
        let x = (0..500).collect();
        let y = sampler.take(500).collect::<Vec<_>>();
        let plot = scatter_plot::<i32, f64>(x, y.clone(), None);
        let mut figure = Figure::new();
        figure.add_plot(plot);
        let mean = mean(&(y.clone()));
        let mut mean_line_opts = LinePlotOptions::new();
        mean_line_opts.line_style = Some(Dash);
        mean_line_opts.color = Some("black".to_string());
        let mean_line = horizontal_line(mean, Some(mean_line_opts));
        figure.add_plot(mean_line);
        print!("{:?}", figure.save("./examples/figures/horizontal_line.png", None, FigureSaveOpts::default()));
    }

    #[test]
    fn line_segment() {
        let x = vec![1, 2, 3, 4];
        let y = vec![0.1, 0.2, 0.5, 0.3];
        let mut options = ScatterPlotOptions::new();
        options.marker = Some(Markers::Diamond);
        let lp = scatter_plot::<i32, f64>(x, y, Some(options));
        let mut figure = Figure::new();
        figure.add_plot(lp);
        let mut line_opt = LinePlotOptions::new();
        line_opt.color = Some("red".to_string());
        line_opt.line_style = Some(LineStyle::Dash);
        figure.add_plot(line::<i32, f64>((1, 0.1), (4, 0.3), Some(line_opt)));
        figure.save("./examples/figures/line_segment.png", None, FigureSaveOpts::default());
        print!("{:?}", figure.script);

    }

}
